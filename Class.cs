using System.Collections.Generic;

namespace Year9DataCollection
{
	public class Class
	{
		public string name { get; set; }
	}

	public class Parent
	{
		public string name { get; set; }
	}

	public class Student
	{
		public List<Parent> parents { get; set; }
		public string Name { get; set; }
		public List<Class> classes { get; set; }
	}

	public class RootObject
	{
		public List<Student> Student { get; set; }
	}
}